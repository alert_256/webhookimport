<?php

namespace Vodopad\Controller\API\REST\V1\Import\Catalog;

use Vodopad\Models\IBlock\Catalog;
use Vodopad\Facades\Section as FacadeSection;
use Vodopad\Facades\IBlockElement;
use Vodopad\Validators\ImportCatalog\ElementValidator;
use Bitrix\Iblock\PropertyTable;
use Bitrix\Iblock\ElementTable;
use Bitrix\Catalog\Model\Price;
use Bitrix\Catalog\GroupTable;


\CModule::IncludeModule('iblock');
\CModule::IncludeModule('catalog');

class Element extends AbstractImport
{

	/**
	 * Массив свойств элементов
	 * @var array
	 */
	protected static $propElements = [];

	/**
	 * Массив значений свойств типа список
	 * @var array
	 */
	protected static $propValEnum = [];

	/**
	 * Массив единиц измерений
	 * @var array
	 */
	protected static $measureList = [];

	/**
	 * Массив цен товаров
	 * @var array
	 */
	protected static $pricesElements = [];

	/**
	 *
	 * @throws \ErrorException
	 * @return array
	 */
	public static function import(): array
	{
		$arData = self::getRequestData();
		try {
			if ($arData) {
				self::setMeasureList();
				foreach ($arData as $field) {
					if (ElementValidator::validate($field)) {
						self::saveElement($field);
					} else {
						self::setErrorStatus('Невалидные поля элемента ' . print_r($field, true));
					}
				}
			}
			self::addLogInfo();
		} catch (\ErrorException $e) {
			self::setErrorStatus($e->getMessage());
		}
		return self::getStatus();
	}

	/**
	 * @param $arFields
	 * @return null
	 */
	protected static function saveElement($arFields)
	{
		$unit = $arFields['MEASURE'];
		unset($arFields['MEASURE']);

		$idElement = IBlockElement::getElementIdByXmlId($arFields['XML_ID']) ?: IBlockElement::getElementIdByXmlId($arFields['XML_ID_1C7']);
		unset($arFields['XML_ID_1C7']);

		$obElement = new \CIBlockElement;
		if ($idElement) {
			$res = $obElement->Update($idElement, $arFields);
		} else {
			$idElement = $obElement->Add($arFields);
		}
		if ($obElement->LAST_ERROR) {
			self::setErrorStatus(print_r($obElement->LAST_ERROR, true));
		} elseif ($idElement) {
			self::saveElementProperty($idElement, $arFields['XML_ID']);
			self::saveCatalogProduct($idElement, $unit);
			self::saveProductPrices($idElement, $arFields['XML_ID']);
			return $idElement;
		}
	}

	protected static function setMeasureList()
	{
		$resMeasure = \CCatalogMeasure::getList();
		while ($measure = $resMeasure->Fetch()) {
			self::$measureList[$measure['CODE']] = $measure['ID'];
		}
	}

	protected static function saveCatalogProduct(int $idElement, $unit)
	{
		$arFieldsProduct = [
			'ID' => $idElement,
		];
		if ($idMeasure = self::$measureList[$unit]) {
			$arFieldsProduct['MEASURE'] = $idMeasure;
		}
		if (!\CCatalogProduct::Add($arFieldsProduct)) {
			self::setErrorStatus('Ошибка обновления параметров товара - ' . $idElement);
		}
	}

	protected static function saveProductPrices(int $idElement, string $xmlId)
	{
		if ($arPrices = self::$pricesElements[$xmlId]) {
			foreach ($arPrices as $price) {
				$arFieldsPrice = Array(
					'PRODUCT_ID' => $idElement,
					'CATALOG_GROUP_ID' => $price['PRICE_ID'],
					'PRICE' => $price['PRICE'],
					'CURRENCY' => $price['CURRENCY'] ?: 'RUB',
				);
				$dbPrice = Price::getList([
					'filter' => array(
						'PRODUCT_ID' => $idElement,
						'CATALOG_GROUP_ID' => $price['PRICE_ID']
					)]);
				if ($arPrice = $dbPrice->fetch()) {
					$result = Price::update($arPrice['ID'], $arFieldsPrice);
					if (!$result->isSuccess()) {
						self::setErrorStatus(print_r($result->getErrorMessages(), true));
					}
				} else {
					$result = Price::add($arFieldsPrice);
					if (!$result->isSuccess()) {
						self::setErrorStatus(print_r($result->getErrorMessages(), true));
					}
				}
			}
		}
	}

	/**
	 * @return array|null
	 */
	protected static function getRequestData()
	{
		$return = null;
		$json = file_get_contents('php://input');
		$entity = json_decode($json, true);
		if ($entity['data']) {
			foreach ($entity['data'] as $arRequest) {
				$idSection = $arRequest['section_guid'] ? FacadeSection::getIdSectionByXmlID($arRequest['section_guid']) : null;
				$return[] = [
					'NAME' => $arRequest['name'],
					'XML_ID' => $arRequest['guid'],
					'IBLOCK_ID' => Catalog::getId(),
					'IBLOCK_SECTION' => $idSection,
					'SORT' => $arRequest['full_code'],
					'PREVIEW_TEXT' => $arRequest['short_description'],
					'DETAIL_TEXT' => $arRequest['description'],

					'XML_ID_1C7' => $arRequest['guid_1c7'],
					'MEASURE' => $arRequest['unit']
				];
				if ($arRequest['properties']) {
					self::setPropElementsData($arRequest, $idSection);
				}
				if ($arRequest['prices']) {
					self::setPricesElementsData($arRequest);
				}
			}
		} else {
			self::setErrorStatus('Неверный формат запроса');
		}
		return $return;
	}

	protected static function setPricesElementsData(array $arRequest)
	{
		foreach ($arRequest['prices'] as $price) {
			$arPrice[$price['guid']] = [
				'PRICE' => $price['value'],
				'CURRENCY' => $price['currency']
			];
		}

		$dbPrice = GroupTable::getList([
			'filter' => array('NAME' => array_keys($arPrice)
			)])->fetchAll();
		foreach ($dbPrice as $price) {
			if ($arPrice[$price['NAME']]) {
				self::$pricesElements[$arRequest['guid']] = [
					'PRICE_ID' => $price['ID'],
					'PRICE' => $arPrice[$price['NAME']]['PRICE'],
					'CURRENCY' => $arPrice[$price['NAME']]['CURRENCY']
				];
			}
		}
	}

	protected static function setPropElementsData(array $arRequest, $idSection)
	{
		$maskType = $idSection && in_array(62605, GetNavChainUp($idSection)) ? 'ucenka' : 'standart';
		$propXmlVal = [
			'MORE_PHOTO' => getImgs($arRequest['unicode'], ['variant' => $maskType]),
			'XML_ID_1C7' => $arRequest['guid_1c7']
		];
		foreach ($arRequest['properties'] as $prop) {
			$propXmlVal[$prop['guid']] = $prop['value'];
		}
		$bitrixPropArr = PropertyTable::getList([
			'filter' => ['XML_ID' => array_keys($propXmlVal), 'IBLOCK_ID' => Catalog::getId()]
		])->fetchAll();

		foreach ($bitrixPropArr as $bitrixProp) {
			if ($valueProp = $propXmlVal[$bitrixProp['XML_ID']]) {
				self::$propElements[$arRequest['guid']][] = [
					'ID_PROP' => $bitrixProp['ID'],
					'XML_ID_PROP' => $bitrixProp['XML_ID'],
					'VALUE' => $valueProp,
					'TYPE_PROP' => $bitrixProp['PROPERTY_TYPE']
				];
			}
		}
	}

	protected static function saveElementProperty(int $idElement, string $xmlIdElement)
	{
		if ($xmlIdElement && $elementProp = self::$propElements[$xmlIdElement]) {
			$arElemProps = [];
			$obProperties = new \CIBlockProperty;
			foreach ($elementProp as $prop) {
				switch ($prop['TYPE_PROP']) {
					case 'L':
						$enumProp = [];
						if (!self::$propValEnum[$prop['XML_ID_PROP']]) {
							$obProps = $obProperties->GetPropertyEnum($prop['ID_PROP'], Array('SORT' => 'ASC'));
							while ($bxEnum = $obProps->Fetch()) {
								$enumProp[$bxEnum['XML_ID']] = $bxEnum['ID'];
							}
							self::$propValEnum[$prop['XML_ID_PROP']] = $enumProp;
						} else {
							$enumProp = self::$propValEnum[$prop['XML_ID_PROP']];
						}
						if (\count($enumProp) > 0 && $enumProp[$prop['VALUE']]) {
							$arElemProps[$prop['ID_PROP']] = $enumProp[$prop['VALUE']];
						}
						break;
					case 'F':
						foreach ($prop['VALUE'] as $file) {
							$arElemProps[$prop['ID_PROP']][] = [
								'VALUE' => CFile::MakeFileArray($_SERVER['DOCUMENT_ROOT'] . $file),
								'DESCRIPTION' => ''
							];
						}
						break;
					default:
						$arElemProps[$prop['ID_PROP']] = $prop['VALUE'];
						break;
				}
			}
			if (\count($arElemProps) > 0) {
				\CIBlockElement::SetPropertyValuesEx($idElement, Catalog::getId(), $arElemProps);
			}
		}
	}
}