<?php
namespace Vodopad\Controller\API\REST\V1\Import\Catalog;

use Monolog\Logger;
use Monolog\Handler\StreamHandler;

abstract class AbstractImport
{
	protected static $status = [
		'status' => 'success',
		'errorMessage' => []
	];

	protected static $logChanel = 'import1c';

	protected static $logDir = '/upload/logimport/';

	public static function setErrorStatus(string $message)
	{
		self::$status['status'] = 'error';
		self::$status['errorMessage'][] = $message;
	}

	public static function getStatus()
	{
		return self::$status;
	}

	public static function addLogInfo()
	{
		$log = new Logger(self::$logChanel);
		$log->pushHandler(new StreamHandler($_SERVER['DOCUMENT_ROOT'] . self::getLogDir() . self::$logChanel . '.log', Logger::WARNING));

		if (self::$status['status'] === 'success') {
			$log->warning('Выгрузка прошла успешно');
		}
		else {
			$log->error(print_r(self::$status['errorMessage'],true));
		}
	}

	protected static function getLogDir()
	{
		return self::$logDir;
	}


	abstract static function import();

}