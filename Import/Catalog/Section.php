<?php

namespace Vodopad\Controller\API\REST\V1\Import\Catalog;

use Vodopad\Models\IBlock\Catalog;
use Vodopad\Facades\Section as FacadeSection;

class Section extends AbstractImport
{
	public static function import()
	{
		$arData = self::getRequestData();
		try {
			if ($arData) {
				foreach ($arData as $field) {
					self::saveSection($field);
				}
			}
			self::addLogInfo();
		}
		catch (\ErrorException $e) {
			self::setErrorStatus($e->getMessage());
		}
		return self::getStatus();
	}

	/**
	 * @param $arFields
	 * @throws \ErrorException
	 */
	protected static function saveSection($arFields)
	{
		$bs = new \CIBlockSection;
		if ($idSection = FacadeSection::getIdSectionByXmlID($arFields['XML_ID'])) {
			$res = $bs->Update($idSection, $arFields);
		} else {
			$id = $bs->Add($arFields);
			$res = ($id > 0);
		}
		if (!$res) {
			self::setErrorStatus($bs->LAST_ERROR);
		}
	}

	/**
	 * @return array|null
	 */
	protected static function getRequestData()
	{
		$return = null;
		$json = file_get_contents('php://input');
		$entity = json_decode($json, true);
		if ($entity['data']) {
			foreach ($entity['data'] as $arRequest) {
				$return[] = [
					'NAME' => $arRequest['name'],
					'XML_ID' => $arRequest['guid'],
					'IBLOCK_ID' => Catalog::getId(),
					'IBLOCK_SECTION_ID' => $arRequest['parentguid'] ? FacadeSection::getIdSectionByXmlID($arRequest['parentguid']) : null,
					'DESCRIPTION' => $arRequest['description'],
				];
			}
		} else {
			self::setErrorStatus('Неверный формат запроса');
		}

		return $return;
	}
}