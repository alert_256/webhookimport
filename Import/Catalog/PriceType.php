<?php

namespace Vodopad\Controller\API\REST\V1\Import\Catalog;
\CModule::IncludeModule('catalog');

class PriceType extends AbstractImport
{
	public static function import()
	{
		$arData = self::getRequestData();
		try {
			if ($arData) {
				foreach ($arData as $field) {
					self::addPriceType($field);
				}
			}
			self::addLogInfo();
		} catch (\ErrorException $e) {
			self::setErrorStatus($e->getMessage());
		}
		return self::getStatus();
	}

	protected static function addPriceType(array $arFields)
	{
		$dbPriceType = \CCatalogGroup::GetList([],
			['NAME' => $arFields['NAME']]
		);
		if (!$arPriceType = $dbPriceType->Fetch()) {
			$ID = \CCatalogGroup::Add($arFields);
			if ($ID <= 0) {
				self::setErrorStatus('Ошибка добавления типа цены - ' . $arFields['NAME']);
			}
		}
	}

	/**
	 * @return array|null
	 */
	protected static function getRequestData()
	{
		$return = null;
		$json = file_get_contents('php://input');
		$entity = json_decode($json, true);
		if ($entity['data']) {
			foreach ($entity['data'] as $arRequest) {
				$return[] = [
					'NAME' => $arRequest['guid'],
					'USER_LANG' => ['ru' => $arRequest['name']],
					'USER_GROUP' => array(2),
					'USER_GROUP_BUY' => array(2),
				];
			}
		} else {
			self::setErrorStatus('Неверный формат запроса');
		}
		return $return;
	}
}