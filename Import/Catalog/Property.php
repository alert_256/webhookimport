<?php

namespace Vodopad\Controller\API\REST\V1\Import\Catalog;

\CModule::IncludeModule('iblock');

class Property extends AbstractImport
{

	protected static $arrPropType = [
		'Число' => 'N',
		'Строка' => 'S',
		'Список' => 'L'
	];

	public static function import()
	{
		$arData = self::getRequestData();
		try {
			if ($arData) {
				foreach ($arData as $field) {
					self::saveProperty($field);
				}
			}
			self::addLogInfo();
		} catch (\ErrorException $e) {
			self::setErrorStatus($e->getMessage());
		}
		return self::getStatus();
	}

	protected static function saveProperty(array $arFields)
	{
		$obProperties = new \CIBlockProperty;
		$properties = CIBlockProperty::GetList(Array(), Array('XML_ID' => $arFields['XML_ID'], 'IBLOCK_ID' => 1));
		if ($prop_fields = $properties->Fetch()) {
			if (!$obProperties->Update($prop_fields['ID'], $arFields))
				self::setErrorStatus($obProperties->LAST_ERROR);
		} else {
			//todo start удалить при первой возможности
			$transName = \Cutil::translit(strtolower($arFields['NAME']), 'ru', ['replace_space' => '_', 'replace_other' => '_']);
			$alias = $transName;
			$pfx_count = 0;
			$repeatFlag = false;
			while (!$repeatFlag) {
				$alias = str_repeat('_', $pfx_count) . $transName;
				$curAliasBx = \CIBlockProperty::GetByID($alias, Catalog::getId());
				$num_rows = $curAliasBx->result->num_rows;
				$repeatFlag = $num_rows == 0;
				$pfx_count++;
			}
			unset($curAliasBx);
			$arFields['CODE'] = $alias;
			//todo end удалить при первой возможности
			if (!$idProp = $obProperties->Add($arFields))
				self::setErrorStatus($obProperties->LAST_ERROR);
		}
	}

	/**
	 * @return array|null
	 */
	protected static function getRequestData()
	{
		$return = null;
		$json = file_get_contents('php://input');
		$entity = json_decode($json, true);
		if ($entity['data']) {
			foreach ($entity['data'] as $arRequest) {

				$values = null;
				if ($arRequest['values']) {
					foreach ($arRequest['values'] as $arValProp) {
						$values[] = [
							'VALUE' => $arValProp['name'],
							'XML_ID' => $arValProp['guid']
						];
					}
				}
				$return[] = [
					'NAME' => $arRequest['name'],
					'XML_ID' => $arRequest['guid'],
					'IBLOCK_ID' => Catalog::getId(),
					'PROPERTY_TYPE' => self::$arrPropType[$arRequest['type']] ?: 'S',
					'VALUES' => $values
				];

			}
		} else {
			self::setErrorStatus('Неверный формат запроса');
		}
		return $return;
	}
}